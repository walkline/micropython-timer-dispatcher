<h1 align="center">MicroPython Timer Dispatcher</h1>

<p align="center"><img src="https://img.shields.io/badge/Licence-MIT-green.svg?style=for-the-badge" /></p>

### 项目介绍

MicroPython 定时器调度管理器，可以使用一个定时器管理多个需要定时运行的任务，以节省有限的定时器资源

### 运行测试代码

开发板进入`repl`模式，运行如下代码即可查看运行结果

```python
import dispatcher

dispatcher.run_test()
```

### 如何使用

```python
from dispatcher import Dispatcher

if __name__ == '__main__':
    def task1():
        print('task1')

    def task2(content):
        print(content)

    tasks = Dispatcher()
    tasks.add_work(task1, 3000)
    tasks.add_work(task2, 5000, 'task2')
```

详细使用方法参考`run_test()`函数

### ~~特别注意~~

~~`ESP32C3`定时器触发回调的间隔时间是实际的**2 倍**，例如，设置回调间隔为`1000ms`的任务，在`ESP32C3`下需要`2000ms`才会调用一次，要解决这个问题，可以在实例化`Dispatcher`时将`adjusting_rate`参数设置为`2`，即：~~

```python
# tasks = Dispatcher(2) # ESP32C3
```

> ~~详情参考 [ESP32C3 Timer period doubled #8084](https://github.com/micropython/micropython/issues/8084)~~

以上问题官方已修复，详情参考 [ports/esp32: Fix ESP32C3 timer period doubling. #10014](https://github.com/micropython/micropython/pull/10014)

### 合作交流

* 联系邮箱：<walkline@163.com>
* QQ 交流群：
	* 走线物联：[163271910](https://jq.qq.com/?_wv=1027&k=xtPoHgwL)
	* 扇贝物联：[31324057](https://jq.qq.com/?_wv=1027&k=yp4FrpWh)

<p align="center"><img src="https://gitee.com/walkline/WeatherStation/raw/docs/images/qrcode_walkline.png" width="300px" alt="走线物联"><img src="https://gitee.com/walkline/WeatherStation/raw/docs/images/qrcode_bigiot.png" width="300px" alt="扇贝物联"></p>
